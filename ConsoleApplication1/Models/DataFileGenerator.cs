﻿using Labs1.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace Labs1.Visualization
{
    public class DataFileGenerator
    {
        public static void GenerateFile(string fileName, List<Connection> connections, List<Point> points, int startPointId)
        {
            List<Node> nodes = new List<Node>();
            List<Edge> edges = new List<Edge>();

            foreach(Point p in points)
            {
                int pointSize = p.Id == startPointId ? 3 : 1; //first point should be bigger
                nodes.Add(new Node(p, pointSize));
            }

            foreach(Connection c in connections)
            {
                edges.Add(new Edge(c));
            }

            edges = edges.GroupBy(x => x.id).Select(x => x.First()).ToList();
            Data data = new Data(nodes, edges);
            string json = JsonConvert.SerializeObject(data, Formatting.Indented);
            File.WriteAllText(fileName + ".json", json);
        }

        public static void GenerateScaterFile(string fileName, List<SimilarityPoint> points)
        {
            string json = JsonConvert.SerializeObject(points, Formatting.Indented);
            File.WriteAllText(fileName + ".json", json);
        }
    }
}