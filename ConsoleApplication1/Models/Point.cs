﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Labs1
{
    public class Point
    {
        public int X { get; set; }
        public int Y { get; set; }

        public int Id { get; set; }

        public int Value { get; set; }

        public Point(int id, int x, int y, int value)
        {
            this.Id = id;
            this.X = x;
            this.Y = y;
            this.Value = value;
        }
    }
}
