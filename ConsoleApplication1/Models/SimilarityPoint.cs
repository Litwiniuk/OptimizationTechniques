﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Labs1.Models
{
    public class SimilarityPoint
    {
        public double X { get; set; }
        public double Y { get; set; }

        public SimilarityPoint(double x, double y)
        {
            this.X = x;
            this.Y = y;
        }
    }
}
