﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Labs1
{
    public class Connection : ICloneable
    {
        public Guid Id { get; private set; }
        public Point From { get; private set; }
        public Point To { get; private set; }
        public double Cost { get; private set; }
        public double Profit { get; set; }

        public Connection(Point from, Point to, double cost)
        {
            if(from.Id == to.Id)
            {
                throw new Exception("Connection from and to the same point!");
            }
            Id = Guid.NewGuid();
            From = from;
            To = to;
            Cost = cost;
            Profit = to.Value - Cost;
        }

        public void Revert()
        {
            Point temp = From;
            Point temp2 = To;

            From = temp2;
            To = temp;
            Profit = To.Value - Cost;
        }

        public object Clone()
        {
            return this.MemberwiseClone();
        }
    }
}
