﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace Labs1.Visualization
{
    public class Data
    {
        public List<Node> nodes { get; set; }
        public List<Edge> edges { get; set; }

        public Data(List<Node> nodes, List<Edge> edges)
        {
            this.nodes = nodes;
            this.edges = edges;
        }
    }

    public class Node
    {
        public string id { get; set; }
        public string label { get; set; }
        public string x { get; set; }
        public string y { get; set; }
        public int size { get; set; }

        public Node (Point p, int size = 1)
        {
            id = p.Id.ToString();
            label = (p.Id + 1).ToString();
            x = p.X.ToString();
            y = p.Y.ToString();
            this.size = size;
        }
    }

    public class Edge
    {
        public string id { get; set; }
        public string source { get; set; }
        public string target { get; set; }

        public Edge(Connection c)
        {
            id = c.From.Id.ToString() + "-" + c.To.Id.ToString();
            source = c.From.Id.ToString();
            target = c.To.Id.ToString();
        }
    }
}