﻿import json
import numpy
files = ["tests.edgeToBest.json.json", "tests.edgeToOthers.json.json",
         "tests.pointToBest.json.json", "tests.pointToOthers.json.json"]

for file in files:
    data = json.load(open(file))
    x = []
    y = []
    for k in data["data"]:
        x.append(k["X"])
        y.append(k["Y"])
    print(file)
    print(numpy.corrcoef(x,y))

    #print(data["data"])
