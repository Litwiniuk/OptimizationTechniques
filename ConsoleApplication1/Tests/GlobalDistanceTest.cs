﻿using Labs1.Algorithms;
using Labs1.Helpers;
using Labs1.Models;
using Labs1.Visualization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Labs1.Tests
{
    class GlobalDistanceTest
    {

        private List<Point> points;
        private double weight;
        double[,] costMatrix;
        List<TimeSpan> times;
        int populationSize;

        public GlobalDistanceTest(List<Point> points, double[,] costMatrix, double weight, int populationSize)
        {
            this.weight = weight;
            this.points = points;
            this.costMatrix = costMatrix;
            this.times = new List<TimeSpan>();
            ResultPresenter.matrix = costMatrix;
            ResultPresenter.points = points;
            this.populationSize = populationSize;
        }

        public List<List<Connection>> GetFirstPopulation()
        {
            List<List<Connection>> result = new List<List<Connection>>();
            for (int i = 0; i < populationSize; i++)
            {
                Console.WriteLine("Generating population. Step {0} of {1}", i + 1, populationSize);
                GenerateRandomCycle rand = new GenerateRandomCycle();
                Dictionary<int, List<Connection>> dictConnections = rand.GetGraphs(points, weight);
                List<List<Connection>> listConnections = dictConnections.Values.ToList();

                foreach (List<Connection> connection in listConnections)
                {
                    LocalSearchAlgorithm lsa = new LocalSearchAlgorithm(this.costMatrix, weight, points, GetGraph(connection), false);
                    result.Add(lsa.Start().First().Value);
                }//for
            }//for
            return result;
        }

        public Dictionary<int, List<Connection>> GetGraph(List<Connection> connections)
        {
            Dictionary<int, List<Connection>> result = new Dictionary<int, List<Connection>>();
            result.Add(connections.First().From.Id, connections);
            return result;
        }

        public List<Connection> GetBest(List<List<Connection>> connections)
        {
            List<Connection> best = new List<Connection>();
            double max = Double.MinValue;
            foreach (List<Connection> con in connections)
            {
                double current = ResultPresenter.CountConnectionsValue(con.First().From.Id, con, weight);
                if (current > max)
                {
                    max = current;
                    best = con;
                }//if
            }//for

            return best;
        }

        public void Start()
        {
            int resultsCounter = 1;

            List<List<Connection>> population = GetFirstPopulation();
            List<Connection> best = GetBest(population);
            List<SimilarityPoint> pointToOthers = new List<SimilarityPoint>();
            List<SimilarityPoint> pointToBest = new List<SimilarityPoint>();
            List<SimilarityPoint> edgeToOthers = new List<SimilarityPoint>();
            List<SimilarityPoint> edgeToBest = new List<SimilarityPoint>();

            foreach (List<Connection> connections1 in population)
            {
                Console.WriteLine("Comparing results. Step {0} of {1}", resultsCounter, population.Count);
                resultsCounter++;

                List<double> pointsSimilatrity = new List<double>();
                List<double> edgeSimilatrity = new List<double>();

                foreach (List<Connection> connections2 in population)
                {
                    if (connections1 == connections2)
                        continue;

                    CommonConnectionsAndPoints ccap = ConnectionsHelper.GetCommonConnectionsAndPoints(connections1, connections2);
                    pointsSimilatrity.Add(
                        ccap.commonPoints.Count / ((connections1.Count + connections2.Count) / 2.0)
                    );

                    edgeSimilatrity.Add(
                        ccap.commonConnections.Count / ((connections1.Count + connections2.Count) / 2.0)
                    );

                    if (connections2 == best)
                    {
                        double connectionValueBest = ResultPresenter.CountConnectionsValue(connections1.First().From.Id, connections1, weight);
                        pointToBest.Add(new SimilarityPoint(
                            connectionValueBest, (ccap.commonPoints.Count / ((connections1.Count + connections2.Count) / 2.0))
                        ));
                        edgeToBest.Add(new SimilarityPoint(
                            connectionValueBest, (ccap.commonConnections.Count / ((connections1.Count + connections2.Count) / 2.0))
                        ));
                    }//if
                }//for

                double connectionValue = ResultPresenter.CountConnectionsValue(connections1.First().From.Id, connections1, weight);
                pointToOthers.Add(new SimilarityPoint(connectionValue, pointsSimilatrity.Sum() / (double) pointsSimilatrity.Count));
                edgeToOthers.Add(new SimilarityPoint(connectionValue, edgeSimilatrity.Sum() / (double) edgeSimilatrity.Count));
            }//for

            DataFileGenerator.GenerateScaterFile("../../Tests/tests.pointToOthers.json", pointToOthers);
            DataFileGenerator.GenerateScaterFile("../../Tests/tests.pointToBest.json", pointToBest);
            DataFileGenerator.GenerateScaterFile("../../Tests/tests.edgeToOthers.json", edgeToOthers);
            DataFileGenerator.GenerateScaterFile("../../Tests/tests.edgeToBest.json", edgeToBest);
        }
    }
}
