﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Labs1
{
    public class Distance
    {
        public static double CountDistance(Point from, Point to)
        {
            return Math.Sqrt(Math.Pow(to.X - from.X, 2) + Math.Pow(to.Y - from.Y, 2));
        }

        public static double CountCost(double[,] matrix, double weight, int firstPoint, int secondPoint)
        {
            return matrix[firstPoint, secondPoint] * weight;
        }

        public static double[,] CountMatrixCost(List<Point> points)
        {
            var matrix = new double[points.Count, points.Count];
            foreach (Point pointFrom in points)
            {
                foreach (Point pointTo in points)
                {
                    matrix[pointFrom.Id, pointTo.Id] = Distance.CountDistance(pointFrom, pointTo);
                }
            }
            return matrix;
        }
    }
}
