﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace Labs1.Helpers
{
    public class ConnectionsHelper
    {
        public static HashSet<int> GetVisitedPoints(List<Point> points, List<Connection> connections)
        {
            HashSet<int> result = new HashSet<int>();
            foreach(Connection c in connections)
            {
                result.Add(points.IndexOf(c.From));
                result.Add(points.IndexOf(c.To));
            }
            return result;
        }

        public static CommonConnectionsAndPoints GetCommonConnectionsAndPoints(List<Connection> connections1, List<Connection> connections2)
        {
            CommonConnectionsAndPoints ccap = new CommonConnectionsAndPoints();
            HashSet<Point> commonPointsSet = new HashSet<Point>();
            HashSet<Point> allPointsSet = GetAllPointsFromConnections(connections1);
            allPointsSet.UnionWith(GetAllPointsFromConnections(connections2));

            foreach (Connection c1 in connections1)
            {
                foreach(Connection c2 in connections2)
                {
                    if( ((c1.From.Id == c2.From.Id) && (c1.To.Id == c2.To.Id) ) ||
                        ((c1.From.Id == c2.To.Id) && (c1.To.Id == c2.From.Id)))
                    {
                        ccap.commonConnections.Add(c1);
                        commonPointsSet.Add(c1.From);
                        commonPointsSet.Add(c1.To);
                        break;
                    }//if
                }//for
            }//for

            ccap.commonPoints = commonPointsSet.ToList();
            ccap.restPoints = allPointsSet.Except(commonPointsSet).ToList();

            return ccap;
        }//GetCommonConnectionsAndPoints()

        private static HashSet<Point> GetAllPointsFromConnections(List<Connection> connections)
        {
            HashSet<Point> pointsSet = new HashSet<Point>();

            foreach(Connection c in connections)
            {
                pointsSet.Add(c.From);
                pointsSet.Add(c.To);
            }//for

            return pointsSet;
        }//GetPointsSet()
    }
}