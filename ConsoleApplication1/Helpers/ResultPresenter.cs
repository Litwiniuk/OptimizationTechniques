﻿using Labs1.Visualization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Labs1.Helpers
{
    public class ResultPresenter
    {
        public static List<Point> points;
        public static double[,] matrix;

        public static void PrepareAndPrintResult(double[,] matrix, Dictionary<int, List<Connection>> connectionsList, List<Point>points, double weight, string fileName, Dictionary<int, TimeSpan> times = null)
        {
            ResultPresenter.points = points;
            ResultPresenter.matrix = matrix;

            Dictionary<int, double> dictionaryStartPointForValue = new Dictionary<int, double>();
            foreach (KeyValuePair<int, List<Connection>> singleConnectionListKeyValue in connectionsList)
            {
                dictionaryStartPointForValue.Add(singleConnectionListKeyValue.Key, CountConnectionsValue(singleConnectionListKeyValue.Key, singleConnectionListKeyValue.Value, weight));
            }

            int pointIdForMaxValue = dictionaryStartPointForValue.OrderByDescending(x => x.Value).First().Key;
            double maxValue = dictionaryStartPointForValue[pointIdForMaxValue];

            int pointIdForMinValue = dictionaryStartPointForValue.OrderBy(x => x.Value).First().Key;
            double minValue = dictionaryStartPointForValue[pointIdForMinValue];

            double average = dictionaryStartPointForValue.Values.Sum() / dictionaryStartPointForValue.Values.Count;

            // support for multiple cycles started from the same point
            int pointIdForMaxValueId = pointIdForMaxValue;
            int pointIdForMinValueId = pointIdForMinValue;
            while (pointIdForMaxValueId > points.Count)
                pointIdForMaxValueId -= points.Count;
            while (pointIdForMinValueId > points.Count)
                pointIdForMinValueId -= points.Count;

            Console.WriteLine("Max value: " + maxValue + ", for start point id: " + points[pointIdForMaxValueId].Id + ", x: " + points[pointIdForMaxValueId].X + ", y: " + points[pointIdForMaxValueId].Y);
            Console.WriteLine("Min value: " + minValue + ", for start point id: " + points[pointIdForMinValueId].Id + ", x: " + points[pointIdForMinValueId].X + ", y: " + points[pointIdForMinValueId].Y);
            Console.WriteLine("Average value: " + average);
            Console.WriteLine("Best result: " + GetStringForConnection(connectionsList[pointIdForMaxValue]));

            if(times != null)
            {
                ShowTimes(times);
            }

            DataFileGenerator.GenerateFile("../../Visualization/" + fileName, connectionsList[pointIdForMaxValue], points, pointIdForMaxValue);
        }

        public static string GetStringForConnection(List<Connection> connections)
        {
            string result = "";
            foreach (Connection c in connections)
            {
                result += $"({c.From.X}, {c.From.Y}), ";
            }

            if (connections.Any())
            {
                result += $"({connections[0].From.X}, {connections[0].From.Y})";
            }

            return result;

        }

        public static double CountConnectionsValue(int startPointId, List<Connection> connections, double weight)
        {
            double result = 0;
            if (connections.Count == 0)
            {
                return points[startPointId].Value;
            }

            foreach (Connection connection in connections)
            {
                result += connection.To.Value - Distance.CountCost(matrix, weight, connection.From.Id, connection.To.Id);
            }

            return result;
        }

        public static void ShowTimes(Dictionary<int, TimeSpan> times)
        {
            TimeSpan maxValue = times.Values.Max();
            TimeSpan minValue = times.Values.Min();

            double doubleAverageTicks = times.Values.Average(timeSpan => timeSpan.Ticks);
            TimeSpan average = new TimeSpan(Convert.ToInt64(doubleAverageTicks));

            Console.WriteLine("Max time: " + maxValue);
            Console.WriteLine("Min time: " + minValue);
            Console.WriteLine("Average time: " + average);
        }
    }
}
