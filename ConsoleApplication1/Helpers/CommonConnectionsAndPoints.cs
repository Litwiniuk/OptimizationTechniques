﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Labs1.Helpers
{
    public class CommonConnectionsAndPoints
    {
        public List<Point> commonPoints;
        public List<Point> restPoints;
        public List<Connection> commonConnections;

        public CommonConnectionsAndPoints()
        {
            commonPoints = new List<Point>();
            restPoints = new List<Point>();
            commonConnections = new List<Connection>();
        }//CommonConnectionsAndPoints()

        public override string ToString()
        {
            String s = "CommonPoints: ";
            foreach(Point p in commonPoints)
            {
                s += p.Id + " ";
            }

            s += "\n RestPoints ";

            foreach (Point p in restPoints)
            {
                s += p.Id + " ";
            }

            s += "\n Connections ";
            foreach(Connection c in commonConnections)
            {
                s += "\n " + c.From.Id + " to " + c.To.Id;
            }
            return s;
        }
    }
}
