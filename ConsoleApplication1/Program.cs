﻿using Labs1.Algorithms;
using Labs1.Helpers;
using Labs1.Tests;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Labs1
{
    class Program
    {
        private static readonly Double weight = 6.0;
        static void Main(string[] args)
        {
            List<Point> pointsList = LoadFile();
            double[,] costMatrix = Distance.CountMatrixCost(pointsList);

            GlobalDistanceTest gdt = new GlobalDistanceTest(pointsList, costMatrix, weight, 10);
            gdt.Start();
        }

        private static void TestGetCommonPoints(List<Point> pointsList)
        {
            List<Connection> con1 = new List<Connection>();
            List<Connection> con2 = new List<Connection>();
            for (int i = 0; i < 5; i++)
            {
                con1.Add(new Connection(pointsList[i], pointsList[i + 1], 0));
            }//for

            for (int i = 3; i < 8; i++)
            {
                con2.Add(new Connection(pointsList[i], pointsList[i + 1], 0));
            }//for

            CommonConnectionsAndPoints ccap = ConnectionsHelper.GetCommonConnectionsAndPoints(con1, con2);
            Console.WriteLine(ccap);
        }

        public static List<Point> LoadFile()
        {
            string mainPath = Directory.GetParent(Directory.GetCurrentDirectory()).Parent.FullName;
            List<Point> pointsList = new List<Point>();
            using (var reader = new StreamReader(mainPath + "/Data/kroA100.tsp"))
            {
                while (!reader.EndOfStream)
                {
                    var line = reader.ReadLine();
                    var values = line.Split(' ');
                    pointsList.Add(new Point(Int32.Parse(values[0]) - 1, Int32.Parse(values[1]), Int32.Parse(values[2]), 0));
                }
            }

            using (var reader = new StreamReader(mainPath + "/Data/kroB100.tsp"))
            {
                int i = 0;
                while (!reader.EndOfStream)
                {
                    var line = reader.ReadLine();
                    Console.WriteLine(line);
                    var values = line.Split(' ');
                    pointsList[i].Value = Int32.Parse(values[1]);
                    i++;
                }
            }
            return pointsList;
        }

        /// <summary>
        /// Generate random cycle
        /// </summary>
        /// <param name="points">List of avaliable points</param>
        /// <param name="returnRandomPointsAmount">If TRUE return random points amount, else use value from pointsToReturn parameter</param>
        /// <param name="pointsToReturn">How many points have to be in cycle. Used only if returnRandomPointsAmount parameter is TRUE</param>
        public static void GenerateRandomCycle(List<Point> pointsList, bool returnRandomPointsAmount = true, int pointsToReturn = 0)
        {
            GenerateRandomCycle randomCycleGenerator = new GenerateRandomCycle();
            List<Connection> connections = randomCycleGenerator.GenerateRandomConnections(pointsList, weight, returnRandomPointsAmount, pointsToReturn);
            Dictionary<int, List<Connection>> connectionsList = new Dictionary<int, List<Connection>>();
            connectionsList.Add(0, connections);
            ResultPresenter.PrepareAndPrintResult(randomCycleGenerator.GetMatrix(), connectionsList, pointsList, weight, "RandomCycleData");
        }
    }
}
