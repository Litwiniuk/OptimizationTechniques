﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Labs1
{
    public class GenerateRandomCycle
    {
        double[,] matrix;
        List<Connection> connections;
        int pointsAmount;

        public double[,] GetMatrix()
        {
            return matrix;
        }//GetMatrix()

        public List<Connection> GetLastGeneratedCycle()
        {
            return connections;
        }//GetLastGeneratedCycle()

        /// <summary>
        /// Returns random cycle
        /// </summary>
        /// <param name="points">List of avaliable points</param>
        /// <param name="weight">Weight - constant to multiply with connection distance</param>
        /// <param name="returnRandomPointsAmount">If TRUE return random points amount, else use value from pointsToReturn parameter</param>
        /// <param name="pointsToReturn">How many points have to be in cycle. Used only if returnRandomPointsAmount parameter is TRUE</param>
        /// <param name="startPointId">Start point id. If null get random point</param>
        /// <returns></returns>
        public List<Connection> GenerateRandomConnections(List<Point> points, double weight, bool returnRandomPointsAmount = true, int pointsToReturn = 0, int? startPointId = null)
        {
            matrix = new double[points.Count, points.Count];

            foreach (Point pointFrom in points)
            {
                foreach (Point pointTo in points)
                {
                    matrix[pointFrom.Id, pointTo.Id] = Distance.CountDistance(pointFrom, pointTo);
                }//for
            }//for

            HashSet<int> selectedPoints = new HashSet<int>();
            if (startPointId != null && startPointId <= points.Count)
                selectedPoints.Add((int)startPointId);
            if (returnRandomPointsAmount)
            {
                do
                {
                    Random random = new Random();
                    pointsAmount = random.Next(points.Count);
                } while (pointsAmount <= 1);
            }//if
            else
                pointsAmount = pointsToReturn;

            if (pointsAmount >= points.Count)
                pointsAmount = points.Count-1;


            HashSet<int> availablePoints = new HashSet<int>(from x in points select x.Id);

            while (selectedPoints.Count < pointsAmount)
            {
                Random random = new Random();
                int p = availablePoints.ElementAt(random.Next(availablePoints.Count));
                availablePoints.Remove(p);
                selectedPoints.Add(p);
            }//while

            connections = new List<Connection>();
            int[] selectedPointsTable = new int[selectedPoints.Count];
            selectedPoints.CopyTo(selectedPointsTable);

            if (pointsAmount <= 1)
                return connections;

            for (int i = 0; i < selectedPointsTable.Length; i++)
            {
                Point from = points.First(p => p.Id == selectedPointsTable[i]);
                Point to;
                if ((i + 1) < selectedPointsTable.Length)
                    to = points.First(p => p.Id == selectedPointsTable[i+1]);
                else
                    to = points.First(p => p.Id == selectedPointsTable[0]);
                Connection con = new Connection(from, to, Distance.CountCost(matrix, weight, from.Id, to.Id));
                connections.Add(con);
            }//for

            return connections;
        }//GetRandomConnections()

        public Dictionary<int, List<Connection>> GetGraphs(List<Point> points, double weight)
        {
            Dictionary<int, List<Connection>> result = new Dictionary<int, List<Connection>>();
            for ( int i = 0; i < 100; i++)
            {
                List<Connection> connections_random = GenerateRandomConnections(points, weight, true, 0, i);
                result[connections_random[0].From.Id] = connections_random;
            }

            return result;
        }
    }
}
