﻿using Labs1.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Labs1.Algorithms
{
    public class GreedyCycleAlgorithmFixed
    {
        Dictionary<int, List<Connection>> connectionsList = new Dictionary<int, List<Connection>>();
        List<Point> points;
        double[,] matrix;

        public int GetMinId(List<Connection> connections, Double weight, HashSet<int> visitedPoints)
        {
            Connection toRemove = null;
            double max = Double.MinValue;
            int minId = -1;
            foreach (Connection c in connections)
            {
                for (int i = 0; i < matrix.GetLength(1); i++)
                {
                    if (visitedPoints.Contains(i))
                    {
                        continue;
                    }

                    double cost = Distance.CountCost(matrix, weight, c.From.Id, i) + Distance.CountCost(matrix, weight, c.To.Id, i);

                    double earning = points[i].Value + Distance.CountCost(matrix, weight, c.From.Id, c.To.Id);

                    double profit = earning - cost;
                    if (profit > max && (profit > 0))
                    {
                        if (cost > earning)
                        {
                            continue;
                        }
                        max = profit;
                        minId = i;
                        toRemove = c;
                    }
                }
            }

            if (minId == -1)
            {
                throw new Exception("No more profitable points!");
            }

            Point newPoint = points.First(x => x.Id == minId);

            connections.Add(new Connection(toRemove.From, newPoint, Distance.CountCost(matrix, weight, toRemove.From.Id, newPoint.Id)));
            connections.Add(new Connection(newPoint, toRemove.To, Distance.CountCost(matrix, weight, newPoint.Id, toRemove.To.Id)));
            connections.Remove(toRemove);

            return minId;
        }

        public Dictionary<int, List<Connection>> Start(List<Point> points, Double weight)
        {
            matrix = new double[points.Count, points.Count];
            this.points = points;
            foreach (Point pointFrom in points)
            {
                foreach (Point pointTo in points)
                {
                    matrix[pointFrom.Id, pointTo.Id] = Distance.CountDistance(pointFrom, pointTo);
                }
            }

            foreach (Point point in points)
            {
                FindConnectionsForPoint(point, weight);

            }

            ResultPresenter.PrepareAndPrintResult(matrix, connectionsList, points, weight, "GreedyCycleData");
            return connectionsList;
        }

        public void FindConnectionsForPoint(Point point, double weight)
        {
            List<Connection> connections = new List<Connection>();
            HashSet<int> visitedPoints = new HashSet<int>() { point.Id };

            Point firstPoint = point;
            Point secondPoint = points.Where(x=>x.Id == GetSecondPoint(firstPoint, weight, visitedPoints)).First();
            visitedPoints.Add(secondPoint.Id);
            connections.Add(new Connection(firstPoint, secondPoint, Distance.CountCost(matrix, weight, firstPoint.Id, secondPoint.Id)));
            connections.Add(new Connection(secondPoint, firstPoint, Distance.CountCost(matrix, weight, secondPoint.Id, firstPoint.Id)));

            int minId = -1;

            while (true)
            {
                try
                {
                    minId = this.GetMinId(connections, weight, visitedPoints);
                }
                catch (Exception)
                {
                    break;
                }
                Point newPoint = this.points.Where(x => x.Id == minId).First();
                visitedPoints.Add(newPoint.Id);
            }

            connectionsList.Add(point.Id, connections);
        }

        public int GetSecondPoint(Point point, Double weight, HashSet<int> visitedPoints)
        {
            double max = Double.MinValue;
            int minId = -1;
            for (int i = 0; i < matrix.GetLength(1); i++)
            {
                if (visitedPoints.Contains(i))
                {
                    continue;
                }

                double cost = Distance.CountCost(matrix, weight, point.Id, i);
                if (points[i].Value - cost > max && (points[i].Value - cost > 0))
                {
                    if (cost * weight < points[i].Value)
                    {
                        continue;
                    }
                    max = points[i].Value - cost;
                    minId = i;
                }
            }

            if (minId == -1)
            {
                throw new Exception("No more profitable points!");
            }

            return minId;
        }
    }
}
