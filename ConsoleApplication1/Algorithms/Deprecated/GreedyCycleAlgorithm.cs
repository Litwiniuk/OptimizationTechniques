﻿using Labs1.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Labs1.Algorithms
{
    public class GreedyCycleAlgorithm
    {
        Dictionary<int, List<Connection>> connectionsList = new Dictionary<int, List<Connection>>();
        List<Point> points;
        double[,] matrix;

        public int GetMinId(Point point, Double weight, HashSet<int> visitedPoints)
        {
            double max = Double.MaxValue;
            int minId = -1;
            for (int i = 0; i < matrix.GetLength(1); i++)
            {
                if (visitedPoints.Contains(i))
                {
                    continue;
                }

                double cost = Distance.CountCost(matrix, weight, point.Id, i);
                if (cost - points[i].Value < max)
                {
                    max = matrix[point.Id, i];
                    minId = i;
                }
            }

            if (minId == -1)
            {
                throw new Exception("No more profitable points!");
            }

            return minId;
        }

        public void Start(List<Point> points, Double weight)
        {
            matrix = new double[points.Count, points.Count];
            this.points = points;
            foreach (Point pointFrom in points)
            {
                foreach (Point pointTo in points)
                {
                    matrix[pointFrom.Id, pointTo.Id] = Distance.CountDistance(pointFrom, pointTo);
                }
            }

            foreach (Point point in points)
            {
                FindConnectionsForPoint(point, weight);

            }

            ResultPresenter.PrepareAndPrintResult(matrix, connectionsList, points, weight, "GreedyCycleData");
        }

        public void FindConnectionsForPoint(Point point, double weight)
        {
            List<Connection> connections = new List<Connection>();
            HashSet<int> visitedPoints = new HashSet<int>() { point.Id };

            Point firstPoint = point;
            Point secondPoint = null;
            int minId = -1;
            while (true)
            {
                try
                {
                    minId = this.GetMinId(firstPoint, weight, visitedPoints);
                }
                catch (Exception)
                {
                    break;
                }
                secondPoint = this.points.Where(x => x.Id == minId).First();
                visitedPoints.Add(secondPoint.Id);
                connections.Add(new Connection(firstPoint, secondPoint, Distance.CountCost(matrix, weight, firstPoint.Id, secondPoint.Id)));
                firstPoint = secondPoint;
            }
            if (connections.Count > 1)
            {
                connections.Add(new Connection(secondPoint, point, Distance.CountCost(matrix, weight, secondPoint.Id, point.Id)));
            }

            connectionsList.Add(point.Id, connections);
        }
    }
}
