﻿using Labs1.Helpers;
using Labs1.Visualization;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;

namespace Labs1.Algorithms
{
    public class MultipleStartLocalSearch
    {
        private List<Point> points;
        private double weight;
        double[,] costMatrix;
        List<TimeSpan> times;

        public MultipleStartLocalSearch(List<Point> points, double[,] costMatrix, double weight)
        {
            this.weight = weight;
            this.points = points;
            this.costMatrix = costMatrix;
            this.times = new List<TimeSpan>();
            ResultPresenter.matrix = costMatrix;
            ResultPresenter.points = points;
        }

        public void Start()
        {
            List<KeyValuePair<Guid, List<Connection>>> results = new List<KeyValuePair<Guid, List<Connection>>>();
            for (int i = 0; i < 1; i++)
            {
                var singleResult = GenerateSingleMultipleStartLocalSearchResult();
                results.Add(singleResult);
            }

            List<KeyValuePair<Guid, double>> listOfMaxValue = new List<KeyValuePair<Guid, double>>();
            foreach(KeyValuePair<Guid,List<Connection>> kv in results)
            {
                listOfMaxValue.Add(new KeyValuePair<Guid, double>(kv.Key, ResultPresenter.CountConnectionsValue(kv.Value.First().From.Id, kv.Value, weight)));
            }

            PrepareResults(results, listOfMaxValue);
        }

        public KeyValuePair<Guid, List<Connection>> GenerateSingleMultipleStartLocalSearchResult()
        {
            Stopwatch sw = new Stopwatch();
            GenerateRandomCycle rand = new GenerateRandomCycle();
            Dictionary<int, List<Connection>> dictConnections = rand.GetGraphs(points, weight);

            LocalSearchAlgorithm localSearch = new LocalSearchAlgorithm(this.costMatrix, this.weight, this.points, dictConnections, false, "multiple-local-search");

            
            sw.Start();
            var result = localSearch.Start();
            sw.Stop();

            times.Add(sw.Elapsed);

            Console.WriteLine("Elapsed={0}", sw.Elapsed);
            return GetMaxEntry(result);
        }

        public KeyValuePair<Guid, List<Connection>> GetMaxEntry(Dictionary<int, List<Connection>> connectionsDict)
        {
            Dictionary<int, double> dictionaryStartPointForValue = new Dictionary<int, double>();
            foreach (KeyValuePair<int, List<Connection>> singleConnectionListKeyValue in connectionsDict)
            {
                dictionaryStartPointForValue.Add(singleConnectionListKeyValue.Key, ResultPresenter.CountConnectionsValue(singleConnectionListKeyValue.Key, singleConnectionListKeyValue.Value, weight));
            }

            int pointIdForMaxValue = dictionaryStartPointForValue.OrderByDescending(x => x.Value).First().Key;
            double maxValue = dictionaryStartPointForValue[pointIdForMaxValue];

            KeyValuePair<Guid, List<Connection>> result = new KeyValuePair<Guid, List<Connection>>(Guid.NewGuid(), connectionsDict[pointIdForMaxValue]);

            return result;
        }

        public void PrepareResults(List<KeyValuePair<Guid, List<Connection>>> results, List<KeyValuePair<Guid, double>> listOfMaxValue)
        {
            Guid maxKeyMultipleLS = listOfMaxValue.OrderByDescending(x => x.Value).First().Key;
            double maxValueMultipleLS = listOfMaxValue.Where(x => x.Key == maxKeyMultipleLS).First().Value;

            Guid minKeyMultipleLS = listOfMaxValue.OrderByDescending(x => x.Value).Last().Key;
            double minValueMultipleLS = listOfMaxValue.Where(x => x.Key == minKeyMultipleLS).Last().Value;

            double average = listOfMaxValue.Sum(x => x.Value) / listOfMaxValue.Count;

            Console.WriteLine("Best result: " + ResultPresenter.GetStringForConnection(results.Where(x => x.Key == maxKeyMultipleLS).First().Value));
            PrintValues(maxValueMultipleLS, minValueMultipleLS, average);

            DataFileGenerator.GenerateFile("../../Visualization/" + "msls.json", results.Where(x=>x.Key == maxKeyMultipleLS).First().Value, points, results.Where(x => x.Key == maxKeyMultipleLS).First().Value.First().From.Id);
        }

        public void PrintValues(double maxValue, double minValue, double average)
        {
            Console.WriteLine("========VALUES=======");
            Console.WriteLine("Max value: " + maxValue.ToString());
            Console.WriteLine("Min value: " + minValue.ToString());
            Console.WriteLine("Average value: " + average.ToString());

            TimeSpan maxValueTime = times.Max();
            TimeSpan minValueTime = times.Min();

            double doubleAverageTicks = times.Average(timeSpan => timeSpan.Ticks);
            TimeSpan averageTime = new TimeSpan(Convert.ToInt64(doubleAverageTicks));

            Console.WriteLine("========TIMES=======");
            Console.WriteLine("Max time: " + maxValueTime);
            Console.WriteLine("Min time: " + minValueTime);
            Console.WriteLine("Average time: " + averageTime);
        }
    }
}