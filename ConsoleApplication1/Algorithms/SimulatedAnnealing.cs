﻿using Labs1.Helpers;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Labs1.Algorithms
{
    public class SimulatedAnnealing
    {
        private int MaxTemp = 3000;
        private int step = 1;
        private List<Point> points;
        private double weight;
        double[,] costMatrix;
        Dictionary<int, TimeSpan> times;
        private readonly TimeSpan maxTime;

        public SimulatedAnnealing(List<Point> points, double[,] costMatrix, double weight)
        {
            this.weight = weight;
            this.points = points;
            this.costMatrix = costMatrix;
            this.times = new Dictionary<int, TimeSpan>();
            ResultPresenter.matrix = costMatrix;
            ResultPresenter.points = points;
        }

        public void Start()
        {
            GenerateRandomCycle randomCycle = new GenerateRandomCycle();
            Dictionary<int, List<Connection>> randomStarts = randomCycle.GetGraphs(points, weight);

            Dictionary<int, List<Connection>> result = new Dictionary<int, List<Connection>>();
            Random rand = new Random();
            var randomNumbers = Enumerable.Range(0, 100).OrderBy(g => rand.NextDouble()).Take(20).ToList();

            foreach (int index in randomNumbers)
            {
                Stopwatch sw = new Stopwatch();
                sw.Start();
                result.Add(index,SingleProcessing(randomStarts[index]));
                sw.Stop();

                times.Add(index, sw.Elapsed);

                Console.WriteLine("Elapsed={0}", sw.Elapsed);
            }

            ResultPresenter.PrepareAndPrintResult(costMatrix, result, points, weight, "SimulatedAnnealing", times);
        }

        public List<Connection> SingleProcessing(List<Connection> connections)
        {
            //TimeSpan currentTime = TimeSpan.FromSeconds;
            List<Connection> currentCycle = connections;
            for (int currentTemp = MaxTemp; currentTemp > 0; currentTemp -= step)
            {
                //if (TimeSpan.Compare(currentTime, maxTime) == 1)
                //{
                //    break;
                //}

                List<Connection> newCycle = GenerateNewCycle(connections);
                double fx = ResultPresenter.CountConnectionsValue(currentCycle.First().From.Id, currentCycle, weight);
                double fy = ResultPresenter.CountConnectionsValue(newCycle.First().From.Id, newCycle, weight);
                if (fx <= fy)
                {
                    currentCycle = newCycle;
                }
                else
                {
                    double ex = Math.Pow(Math.E, ((fy - fx) / currentTemp));
                    double rn = GetRandomNumber(0, 1);
                    if (ex > rn)
                    {
                        currentCycle = newCycle;
                    }
                }
            }
            return currentCycle;
        }

        public List<Connection> GenerateNewCycle(List<Connection> currentGraph)
        {
            List<Connection> result = new List<Connection>();
            HashSet<int> visitedPoints = ConnectionsHelper.GetVisitedPoints(points, currentGraph);
            HashSet<int> notVisitedPoints = new HashSet<int>(Enumerable.Range(0, 100).Where(x => !visitedPoints.Contains(x)).ToList());

            foreach (Connection c in currentGraph)
            {
                result.Add(new Connection(c.From, c.To, c.Cost));
            }

            CheckIsGraphCorrect(result);

            for (int i = 1; i < (int)GetRandomNumber(0, 3); i++)
            {
                RandomAddPoint(result, visitedPoints, notVisitedPoints);
            }

            //for (int i = 0; i < (int)GetRandomNumber(0, 3); i++)
            //{
            //    RandomReplaceConnections(result);
            //}

            for (int i = 0; i < (int)GetRandomNumber(0, 1); i++)
            {
                RandomRemovePoint(result, visitedPoints, notVisitedPoints);
            }

            return result;
        }

        public double GetRandomNumber(double minimum, double maximum)
        {
            Random random = new Random();
            return random.NextDouble() * (maximum - minimum) + minimum;
        }

        public void RandomAddPoint(List<Connection> connections, HashSet<int> visitedPoints, HashSet<int> notVisitedPoints)
        {
            if (notVisitedPoints.Count == 0 || connections.Count == 0)
            {
                return;
            }

            int randomNumber = (int)GetRandomNumber(0, notVisitedPoints.Count);

            Point newPoint = points.Where(x => x.Id == notVisitedPoints.ElementAt(randomNumber)).First();

            var randomConnection = (int)GetRandomNumber(0, connections.Count);

            Connection connectionToDelete = connections.ElementAt(randomConnection);
            try
            {
                connections.Add(new Connection(connectionToDelete.From, newPoint, Distance.CountCost(costMatrix, weight, connectionToDelete.From.Id, newPoint.Id)));
                connections.Add(new Connection(newPoint, connectionToDelete.To, Distance.CountCost(costMatrix, weight, newPoint.Id, connectionToDelete.To.Id)));
            }
            catch(Exception e)
            {
                var w = e;
            }
            connections.Remove(connectionToDelete);
           

            visitedPoints.Add(newPoint.Id);
            notVisitedPoints.Remove(newPoint.Id);


        }

        public void RandomRemovePoint(List<Connection> connections, HashSet<int> visitedPoints, HashSet<int> notVisitedPoints)
        {
            if (visitedPoints.Count == 0 || connections.Count == 0)
            {
                return;
            }

            int randomNumber = (int)GetRandomNumber(0, visitedPoints.Count);

            Point pointToDelete = points.Where(x => x.Id == visitedPoints.ElementAt(randomNumber)).First();

            IEnumerable<Connection> pointConnections = connections.Where(x => x.From == pointToDelete || x.To == pointToDelete).Distinct();

            if (pointConnections.First() == pointConnections.Last())
            {
                return;
            }

            List<Point> pointsToStay = PointsForDeletedConnection(pointConnections.First(), pointConnections.Last(), pointToDelete);
            visitedPoints.Remove(pointToDelete.Id);
            notVisitedPoints.Add(pointToDelete.Id);

            if (connections.Count == 2)
            {
                connections.RemoveAll(x => x.From.Id == pointToDelete.Id || x.To.Id == pointToDelete.Id);
                return;
            }

            Connection newConnection = new Connection(pointsToStay.First(), pointsToStay.Last(), Distance.CountCost(costMatrix, weight, pointsToStay.First().Id, pointsToStay.Last().Id));

            //connections.RemoveAll(x => x.From.Id == tempPoint.Id || x.To.Id == tempPoint.Id);
            connections.Remove(pointConnections.First());
            connections.Remove(pointConnections.Last());
            if (connections.Where(x => x.From == newConnection.From && x.To == newConnection.To).Any())
            {
                Point from = newConnection.From;
                Point to = newConnection.To;
                newConnection = new Connection(to, from, Distance.CountCost(costMatrix, weight, to.Id, from.Id));
            }
            connections.Add(newConnection);
        }

        public void RandomReplaceConnections(List<Connection> connections)
        {
 
            Console.WriteLine("REPLACE START");
            if (connections.Count < 4)
            {
                return;
            }

            int randomConnection = (int)GetRandomNumber(0, connections.Count);
            int randomConnection2 = (int)GetRandomNumber(0, connections.Count);

            while (randomConnection2 == randomConnection)
            {
                randomConnection2 = (int)GetRandomNumber(0, connections.Count);
            }

            Connection firstConnection = connections[randomConnection];
            Connection secondConnection = connections[randomConnection2];

            if (firstConnection == secondConnection || firstConnection.To == secondConnection.To || firstConnection.From == secondConnection.From ||
                          firstConnection.From == secondConnection.To || secondConnection.To == secondConnection.From || secondConnection.Id == firstConnection.Id)
            {
                return;
            }

            Connection newFirstConnection = new Connection(firstConnection.From, secondConnection.From, Distance.CountCost(costMatrix, weight, firstConnection.From.Id, secondConnection.From.Id)); //tempConn
            Connection newSecondConnection = new Connection(firstConnection.To, secondConnection.To, Distance.CountCost(costMatrix, weight, firstConnection.To.Id, secondConnection.To.Id)); //tempConn2

            connections.Remove(firstConnection);
            connections.Remove(secondConnection);

            connections.Add(newFirstConnection);
            connections.Add(secondConnection);

            
            RevertCycle(connections, newFirstConnection, newSecondConnection);

        }

        public void RevertCycle(List<Connection> connections, Connection tempConn, Connection tempConn2)
        {
            Point from = tempConn.To;
            Point to = tempConn2.From;

            Point temp = from;
            while (true)
            {
                Connection c = connections.Where(x => x.To.Id == temp.Id).First();
                //   c.Revert();
                Connection newConnection = new Connection(c.To, c.From, c.Cost);
                connections.Remove(c);
                connections.Add(newConnection);
                temp = newConnection.To;
                //  temp = c.To;
                if (temp.Id == to.Id)
                {
                    break;
                }
            }
        }

        public void CheckIsGraphCorrect(List<Connection> connections)
        {
            int startPointId = connections[0].From.Id;
            int numbersInCycle = connections.Count;
            int temp = startPointId;
            int counter = 0;
            List<Connection> visited = new List<Connection>();
            while (true)
            {
                Connection connectionTemp = connections.Where(x => x.From.Id == temp).First();
                visited.Add(connectionTemp);
                temp = connectionTemp.To.Id;

                counter++;
                if (temp == startPointId || counter > numbersInCycle* 2)
                {
                    break;
                }
            }

            if (counter != numbersInCycle)
            {
                throw new Exception("Invalid graph!");
            }
        }

        public List<Point> PointsForDeletedConnection(Connection firstConnection, Connection secondConnection, Point toDelete)
        {
            return firstConnection.From.Id == toDelete.Id ? new List<Point>() { secondConnection.From, firstConnection.To } : new List<Point>() { firstConnection.From, secondConnection.To };
        }
    }
}
