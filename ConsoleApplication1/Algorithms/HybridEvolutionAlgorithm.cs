﻿using Labs1.Helpers;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Labs1.Algorithms
{
    public class HybridEvolutionAlgorithm
    {
        private List<Point> points;
        private double weight;
        double[,] costMatrix;
        List<TimeSpan> times;
        TimeSpan executionMaxTime;

        public HybridEvolutionAlgorithm(List<Point> points, double[,] costMatrix, double weight, TimeSpan executionMaxTime)
        {
            this.weight = weight;
            this.points = points;
            this.costMatrix = costMatrix;
            this.times = new List<TimeSpan>();
            ResultPresenter.matrix = costMatrix;
            ResultPresenter.points = points;
            this.executionMaxTime = executionMaxTime;
        }

        public void Start()
        {
            Console.WriteLine("Generating first population");
            List<List<Connection>> population = GetFirstPopulation();
            int numberOfLocalSearch = 0;
            Stopwatch sw = new Stopwatch();
            List<TimeSpan> times = new List<TimeSpan>();

            int iterations = 0;
            int allSolutions = 0;
            int acceptedSolutions = 0;

            Console.WriteLine("Hybrid evolution algorithm is started");
            sw.Start();

            while (sw.Elapsed < executionMaxTime)
            {
                iterations++;
                Random rand = new Random();
                #region pick 2 random connections
                int firstRandomNumber= rand.Next(20);
                int secondRandomNumber = rand.Next(20);
                while (firstRandomNumber == secondRandomNumber)
                    secondRandomNumber = rand.Next(10);
                #endregion

                List<Connection> firstRandomResult = population[firstRandomNumber];
                List<Connection> secondRandomResult = population[secondRandomNumber];

                List<Connection> recombineResult = Recombination(firstRandomResult, secondRandomResult);
                // skip empty result
                if (recombineResult == null)
                    continue;

                LocalSearchAlgorithm lsa = new LocalSearchAlgorithm(costMatrix, weight, points, GetGraph(recombineResult), false);
                List<Connection> recombineAfterLocalSearch = lsa.Start().First().Value;
                numberOfLocalSearch++;

                List<Connection> worstGraph = FindWorstInPopulation(population);

                if(ResultPresenter.CountConnectionsValue(recombineAfterLocalSearch.First().From.Id, recombineAfterLocalSearch, weight) > ResultPresenter.CountConnectionsValue(worstGraph.First().From.Id, worstGraph, weight))
                {
                    if (IsUnique(population, recombineAfterLocalSearch))
                    {
                        population.Remove(worstGraph);
                        population.Add(recombineAfterLocalSearch);
                        acceptedSolutions++;
                    }//if
                }//if

                allSolutions++;
                times.Add(sw.Elapsed);
            }//while

            sw.Stop();
            Console.WriteLine("All iterations: " + iterations);
            Console.WriteLine("All solutions: " + allSolutions);
            Console.WriteLine("Accepted solutions: " + acceptedSolutions);
            Dictionary<int, TimeSpan> measuredTimes = PrepareTimes(times);
            ResultPresenter.PrepareAndPrintResult(costMatrix, ConvertConnectionsListsToDictionary(population), points, weight, "hybridEvolutionAlgorithmData", measuredTimes);
        }

        private Dictionary<int, TimeSpan> PrepareTimes(List<TimeSpan> times)
        {
            Dictionary<int, TimeSpan> measuredTimes = new Dictionary<int, TimeSpan>();
            TimeSpan last = new TimeSpan(0, 0, 0);
            for(int i = 0; i < times.Count; i++)
            {
                measuredTimes.Add(i, times[i] - last);
                last = times[i];
            }//for

            return measuredTimes;
        }

        private bool IsUnique(List<List<Connection>> population, List<Connection> recombineAfterLocalSearch)
        {
            foreach(List<Connection> fromPopulation in population)
            {
                if(fromPopulation.Count == recombineAfterLocalSearch.Count)
                {
                    double fromPopulationValue = ResultPresenter.CountConnectionsValue(
                        fromPopulation.First().From.Id, fromPopulation, weight);
                    double recombinationValue = ResultPresenter.CountConnectionsValue(
                        recombineAfterLocalSearch.First().From.Id, recombineAfterLocalSearch, weight);
                    if(fromPopulationValue == recombinationValue)
                    {
                        return false;
                    }//if
                }//if
            }//for
            return true;
        }

        private Dictionary<int, List<Connection>> ConvertConnectionsListsToDictionary(List<List<Connection>> population)
        {
            Dictionary<int, List<Connection>> connectionsDict = new Dictionary<int, List<Connection>>();
            for(int i = 0; i < population.Count; i++)
            {
                int key = population[i].First().From.Id;
                while (connectionsDict.ContainsKey(key))
                    key += 100; // protect against key duplicates
                connectionsDict.Add(key, population[i]);
            }//for

            return connectionsDict;
        }

        private List<Connection> Recombination(List<Connection> firstRandomResult, List<Connection> secondRandomResult)
        {
            SortConnectionsByPointsCounterDesc(ref firstRandomResult, ref secondRandomResult);
            int recombinationResultPoints = new Random().Next(firstRandomResult.Count, secondRandomResult.Count);

            CommonConnectionsAndPoints ccap = ConnectionsHelper.GetCommonConnectionsAndPoints(firstRandomResult, secondRandomResult);
            if (ccap.commonConnections.Count == 0)
                return null; // there is no common edges in two graphs

            //ResultPresenter.PrepareAndPrintResult(costMatrix, GetGraph(ccap.commonConnections), points, weight, "commonConnections");
            List<Connection> recombinationResult = CreateCycleFromConnections(ccap.commonConnections);
            recombinationResult = AddAdditionalPointsToFillResult(recombinationResult, ccap.restPoints, recombinationResultPoints);

            return recombinationResult;
        }

        private List<Connection> AddAdditionalPointsToFillResult(List<Connection> recombinationResult, List<Point> restPoints, int recombinationResultPoints)
        {
            if (recombinationResultPoints <= recombinationResult.Count)
                return recombinationResult;

            int pointsToAddCounter = recombinationResultPoints - recombinationResult.Count;
            for(int i = 0; i < pointsToAddCounter; i++)
            {
                AddRandomPointToRandomEdge(recombinationResult, restPoints);
            }//for

            return recombinationResult;
        }

        private void AddRandomPointToRandomEdge(List<Connection> recombinationResult, List<Point> restPoints)
        {
            int pointIndex = new Random().Next(restPoints.Count - 1);
            Point p = restPoints[pointIndex];
            int connectionIndex = new Random().Next(recombinationResult.Count - 1);
            Connection toRemove = recombinationResult[connectionIndex];

            Connection toPoint = new Connection(toRemove.From, p, Distance.CountDistance(toRemove.From, p) * weight);
            Connection fromPoint = new Connection(p, toRemove.To, Distance.CountDistance(p, toRemove.To) * weight);

            recombinationResult.Remove(toRemove);
            recombinationResult.Insert(connectionIndex, toPoint);
            recombinationResult.Insert(connectionIndex + 1, fromPoint);
            restPoints.Remove(p);
        }

        private List<Connection> CreateCycleFromConnections(List<Connection> commonConnections)
        {
            List<Connection> cycle = new List<Connection>();
            List<List<Connection>> groupedConnections = GroupConnections(commonConnections);

            while(groupedConnections.Count >= 2)
            {
                List<Connection> firstConnections = GetAndRemoveRandomConnectionsGroup(groupedConnections);
                List<Connection> secondConnections = GetAndRemoveRandomConnectionsGroup(groupedConnections);

                // connect firstConnections to current connections
                if(cycle.Count > 0)
                {
                    Point fromCycle = cycle[cycle.Count - 1].To;
                    Point toFirst = firstConnections[0].From;
                    if (fromCycle.Id != toFirst.Id)
                    {
                        Connection betweenCycleAndFirst = new Connection(fromCycle, toFirst,
                            Distance.CountCost(costMatrix, weight, fromCycle.Id, toFirst.Id));
                        cycle.Add(betweenCycleAndFirst);
                    }//if
                }//if
                foreach(Connection c in firstConnections)
                    cycle.Add(c);

                // add connection between two groups
                Point from = firstConnections[firstConnections.Count - 1].To;
                Point to = secondConnections[0].From;
                if (from.Id != to.Id)
                {
                    Connection between = new Connection(from, to, Distance.CountCost(costMatrix, weight, from.Id, to.Id));
                    cycle.Add(between);
                }//if

                foreach (Connection c in secondConnections)
                    cycle.Add(c);
            }//while

            if(groupedConnections.Count == 1)
            {
                List<Connection> connections = groupedConnections[0];
                if (cycle.Count > 0)
                {
                    Connection lastCycle = cycle[cycle.Count - 1];
                    Connection firstGrouped = connections[0];
                    if (lastCycle.To.Id != firstGrouped.From.Id)
                    {
                        Connection between = new Connection(lastCycle.To, firstGrouped.From,
                            Distance.CountCost(costMatrix, weight, lastCycle.To.Id, firstGrouped.From.Id));
                        cycle.Add(between);
                    }//if
                }//if

                foreach (Connection c in connections)
                    cycle.Add(c);
            }//if

            // Add closing connection
            Connection last = cycle[cycle.Count - 1];
            Connection first = cycle[0];
            if (last.To.Id != first.From.Id)
            {
                Connection between = new Connection(last.To, first.From,
                    Distance.CountCost(costMatrix, weight, last.To.Id, first.From.Id));
                cycle.Add(between);
            }//if

            return cycle;
        }

        private List<Connection> GetAndRemoveRandomConnectionsGroup(List<List<Connection>> groupedConnections)
        {
            List<Connection> connections = groupedConnections[new Random().Next(groupedConnections.Count - 1)];
            groupedConnections.Remove(connections);
            return connections;
        }

        private List<List<Connection>> GroupConnections(List<Connection> commonConnections)
        {
            List<List<Connection>> groupedConnections = new List<List<Connection>>();
            foreach (Connection connection in commonConnections)
            {
                bool found = false;
                foreach (List<Connection> entry in groupedConnections)
                {
                    Connection con = entry[0];
                    if (connection.To.Id == con.From.Id)
                    {
                        entry.Insert(0, connection);
                        found = true;
                        break;
                    }//if

                    con = entry[entry.Count - 1];
                    if (connection.From.Id == con.To.Id)
                    {
                        entry.Add(connection);
                        found = true;
                        break;
                    }//if
                }//for

                if (found)
                    continue;

                List<Connection> connections = new List<Connection>();
                connections.Add(connection);
                groupedConnections.Add(connections);
            }//for
            
            // regroup connections to be sure that everything is ok
            int i = 0;
            while(i < groupedConnections.Count)
            {
                bool found = false;
                List<Connection> currentConnectionsList = groupedConnections[i];
                for(int j = 0; j < groupedConnections.Count; j++)
                {
                    if (j == i)
                        continue;

                    List<Connection> currentAnalisingConnections = groupedConnections[j];
                    if(currentConnectionsList[currentConnectionsList.Count - 1].To.Id == currentAnalisingConnections[0].From.Id)
                    {
                        found = true;
                        currentConnectionsList.AddRange(currentAnalisingConnections);
                        groupedConnections.RemoveAt(j);
                        i = 0;
                        break;
                    }//if
                }//for

                if (found == false)
                    i++;
            }//while
            
            return groupedConnections;
        }

        private void SortConnectionsByPointsCounterDesc(ref List<Connection> firstRandomResult, ref List<Connection> secondRandomResult)
        {
            if(firstRandomResult.Count > secondRandomResult.Count)
            {
                List<Connection> temp = firstRandomResult;
                firstRandomResult = secondRandomResult;
                secondRandomResult = temp;
            }//if
        }

        public List<List<Connection>> GetFirstPopulation()
        {
            GenerateRandomCycle rand = new GenerateRandomCycle();
            Dictionary<int, List<Connection>> dictConnections = rand.GetGraphs(points, weight);
            List<List<Connection>>listConnections =  dictConnections.Values.Take(20).ToList();

            List<List<Connection>> result = new List<List<Connection>>();
            foreach (List<Connection> connection in listConnections)
            {
                LocalSearchAlgorithm lsa = new LocalSearchAlgorithm(this.costMatrix, weight, points, GetGraph(connection), false);
                result.Add(lsa.Start().First().Value);
            }
            return result;
        }

        public Dictionary<int, List<Connection>> GetGraph(List<Connection> connections)
        {
            Dictionary<int, List<Connection>> result = new Dictionary<int, List<Connection>>();
            result.Add(connections.First().From.Id, connections);
            return result;
        }

        public List<Connection> FindWorstInPopulation(List<List<Connection>> connections)
        {
            int indexWorst = 0;
            double worstValue = Double.MaxValue;

            for(int i = 0; i< connections.Count; i++)
            {
                double value = ResultPresenter.CountConnectionsValue(connections[i].First().From.Id, connections[i], weight);
                if(value < worstValue)
                {
                    worstValue = value;
                    indexWorst = i;
                }
            }
            return connections[indexWorst];
        }
    }
}
