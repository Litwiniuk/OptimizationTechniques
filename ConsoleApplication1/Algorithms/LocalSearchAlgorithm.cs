﻿using Labs1.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;

namespace Labs1.Algorithms
{
    public class LocalSearchAlgorithm
    {
        private static readonly bool checkIsGraphCorrect = false;
        private bool saveToFile = true;
        private string algorithmName;
        private double maxProfit = Double.MinValue;
        private double[,] costMatrix;
        private double weight;
        List<Point> points;
        Action action;

        Connection tempConn;
        Connection tempConn2;

        Connection tempConn3;
        Connection tempConn4;
        Point tempPoint;

        Dictionary<int, List<Connection>> connectionsList = new Dictionary<int, List<Connection>>();

        public LocalSearchAlgorithm(double[,] costMatrix, double weight, List<Point> points, Dictionary<int, List<Connection>> connectionsList, bool saveToFile, string algorithmName = "")
        {
            this.weight = weight;
            this.costMatrix = costMatrix;
            this.connectionsList = connectionsList;
            this.points = points;
            this.algorithmName = algorithmName;
            this.saveToFile = saveToFile;
        }

        public Dictionary<int, List<Connection>> Start()
        {
            Dictionary<int, List<Connection>> result = new Dictionary<int, List<Connection>>();
            Dictionary<int, TimeSpan> times = new Dictionary<int, TimeSpan>();
            foreach (KeyValuePair<int, List<Connection>> entry in connectionsList)
            {
                Stopwatch sw = new Stopwatch();
                sw.Start();
                var updatedConnections = ProcesSingleList(entry.Value, ConnectionsHelper.GetVisitedPoints(points, entry.Value), entry.Key);
                sw.Stop();
                times.Add(entry.Key, sw.Elapsed);
             //   Console.WriteLine("Elapsed={0}", sw.Elapsed + " key: " + entry.Key);
                result.Add(entry.Key, updatedConnections);
            }
            if (saveToFile)
            {
                ResultPresenter.PrepareAndPrintResult(costMatrix, connectionsList, points, weight, "LocalSearch_" + algorithmName, times);
            }
            return result;
        }

        public List<Connection> ProcesSingleList(List<Connection> connections, HashSet<int> visitedPoints, int startPointId)
        {
            bool stayInLoop = true;
            while (stayInLoop)
            {
                //    CheckIsGraphCorrect(connections);
                maxProfit = Double.MinValue;
                action = Action.None;
                foreach (Connection connection1 in connections)
                {
                    foreach (Connection connection2 in connections)
                    {
                        //skip the same edge and neighbours
                        if (connection1 == connection2 || connection1.To == connection2.To || connection1.From == connection2.From ||
                            connection1.From == connection2.To || connection1.To == connection2.From || connection2.Id == connection1.Id)
                        {
                            continue;
                        }

                        if (connections.Count < 4)
                        {
                            continue;
                        }

                        ReplaceTwoEdge(connection1, connection2);
                    }
                }


                foreach (Point point in points)
                {
                    if (visitedPoints.Contains(point.Id))
                    {
                        if (point.Id == startPointId)
                        {
                            // continue;
                        }
                        IEnumerable<Connection> pointConnections = connections.Where(x => x.From == point || x.To == point).Distinct();
                        if (pointConnections.First() == pointConnections.Last())
                        {
                            continue;
                        }
                        RemovePoint(point, pointConnections.First(), pointConnections.Last()); // todo: replace
                    }
                    else
                    {
                        foreach (Connection connection1 in connections)
                        {
                            AddNewPoint(connection1, point);
                        }
                    }
                }


                if (maxProfit < 0)
                {
                    stayInLoop = false;
                    break;
                }

                switch (action)
                {
                    case Action.ReplaceEdges:
                        DoReplaceEdges(connections);
                        break;
                    case Action.AddPoint:
                        DoAddNewPoint(connections, visitedPoints);
                        break;
                    case Action.RemovePoint:
                        DoRemovePoint(connections, visitedPoints);
                        break;
                    case Action.None:
                    default:
                        break;
                }

                #region clear unused variable
                tempConn = null;
                tempConn2 = null;

                tempConn3 = null;
                tempConn4 = null;
                tempPoint = null;
                #endregion
            }

            return connections;
        }

        public void AddNewPoint(Connection c, Point p)
        {
            double cost = Distance.CountCost(costMatrix, weight, c.From.Id, p.Id) + Distance.CountCost(costMatrix, weight, p.Id, c.To.Id);

            double profit = p.Value + c.Cost - cost;

            if (profit > 0 && profit > maxProfit)
            {
                maxProfit = profit;
                action = Action.AddPoint;
                tempConn = c;
                tempPoint = p;

                #region clean unused variable
                tempConn2 = null;
                tempConn3 = null;
                tempConn4 = null;
                #endregion
            }
        }

        public void RemovePoint(Point toRemove, Connection connection1, Connection connection2)
        {
            List<Point> pointsToStay = new List<Point>() { connection1.To, connection1.From, connection2.From, connection2.To }.Where(x => x.Id != toRemove.Id).Distinct().ToList();

            if (pointsToStay.First() == pointsToStay.Last())
            {
                return;
            }

            double profit = 0;
            if (pointsToStay.Count == 0)
            {
                profit = toRemove.Value - connection1.Cost;
            }
            else
            {
                Connection newConnection = new Connection(pointsToStay.First(), pointsToStay.Last(), Distance.CountCost(costMatrix, weight, pointsToStay.First().Id, pointsToStay.Last().Id));
                profit = (connection1.Cost + connection2.Cost) - toRemove.Value - newConnection.Cost;
            }

            if (profit > 0 && profit > maxProfit)
            {
                maxProfit = profit;
                action = Action.RemovePoint;

                tempPoint = toRemove;
                tempConn = connection1;
                tempConn2 = connection2;

                #region clean unused variable
                tempConn3 = null;
                tempConn4 = null;
                #endregion
            }
        }

        public void ReplaceTwoEdge(Connection firstConnection, Connection secondConnection)
        {
            double profit = 0;

            Connection newFirstConnection = new Connection(firstConnection.From, secondConnection.From, Distance.CountCost(costMatrix, weight, firstConnection.From.Id, secondConnection.From.Id));
            Connection newSecondConnection = new Connection(firstConnection.To, secondConnection.To, Distance.CountCost(costMatrix, weight, firstConnection.To.Id, secondConnection.To.Id));
            double cost = firstConnection.Cost + secondConnection.Cost;
            profit = cost - newFirstConnection.Cost - newSecondConnection.Cost;

            if (profit > 0 && profit > maxProfit)
            {
                maxProfit = profit;
                action = Action.ReplaceEdges;

                //connections to add
                tempConn = newFirstConnection;
                tempConn2 = newSecondConnection;

                //connections to remove
                tempConn3 = firstConnection;
                tempConn4 = secondConnection;

                #region clean unused variable
                tempPoint = null;
                #endregion
            }
        }

        public void DoAddNewPoint(List<Connection> connections, HashSet<int> visitedPoints)
        {
           // Console.WriteLine("ADD NEW");
            visitedPoints.Add(tempPoint.Id);
            connections.Remove(tempConn);
            Connection connectionNew = new Connection(tempConn.From, tempPoint, Distance.CountCost(costMatrix, weight, tempConn.From.Id, tempPoint.Id));
            Connection connectionNew2 = new Connection(tempPoint, tempConn.To, Distance.CountCost(costMatrix, weight, tempPoint.Id, tempConn.To.Id));
            connections.Add(connectionNew);
            connections.Add(connectionNew2);
        }

        public void DoRemovePoint(List<Connection> connections, HashSet<int> visitedPoints)
        {
        //    Console.WriteLine("REMOVE");
            List<Point> pointsToStay = PointsForDeletedConnection(tempConn, tempConn2, tempPoint);
            visitedPoints.Remove(tempPoint.Id);

            if (connections.Count == 2)
            {
                connections.RemoveAll(x => x.From.Id == tempPoint.Id || x.To.Id == tempPoint.Id);
                return;
            }

            Connection newConnection = new Connection(pointsToStay.First(), pointsToStay.Last(), Distance.CountCost(costMatrix, weight, pointsToStay.First().Id, pointsToStay.Last().Id));

            //connections.RemoveAll(x => x.From.Id == tempPoint.Id || x.To.Id == tempPoint.Id);
            connections.Remove(tempConn);
            connections.Remove(tempConn2);
            if (connections.Where(x => x.From == newConnection.From && x.To == newConnection.To).Any())
            {
                Point from = newConnection.From;
                Point to = newConnection.To;
                newConnection = new Connection(to, from, Distance.CountCost(costMatrix, weight, to.Id, from.Id));
            }
            connections.Add(newConnection);
        }

        public void DoReplaceEdges(List<Connection> connections)
        {
        //    CheckIsGraphCorrect(connections);
        //    Console.WriteLine("REPLACE");
            connections.Remove(tempConn3);
            connections.Remove(tempConn4);

            connections.Add(tempConn);
            connections.Add(tempConn2);
            RevertCycle(connections);
        //    CheckIsGraphCorrect(connections);

        }

        public void RevertCycle(List<Connection> connections)
        {
            Point from = tempConn.To;
            Point to = tempConn2.From;

            Point temp = from;
            while (true)
            {
                Connection c = connections.Where(x => x.To.Id == temp.Id).First();
                //   c.Revert();
                Connection newConnection = new Connection(c.To, c.From, c.Cost);
                connections.Remove(c);
                connections.Add(newConnection);
                temp = newConnection.To;
                //  temp = c.To;
                if (temp.Id == to.Id)
                {
                    break;
                }
            }
        }

        public static void CheckIsGraphCorrect(List<Connection> connections)
        {
            //todo:
            if(!LocalSearchAlgorithm.checkIsGraphCorrect)
                return;

            //
            int startPointId = connections[0].From.Id;
            int numbersInCycle = connections.Count;
            int temp = startPointId;
            int counter = 0;
            List<Connection> visited = new List<Connection>();
            while (true)
            {
                Connection connectionTemp = connections.Where(x => x.From.Id == temp).First();
                visited.Add(connectionTemp);
                temp = connectionTemp.To.Id;

                counter++;
                if (temp == startPointId)
                {
                    break;
                }
            }

            if (counter != numbersInCycle)
            {
                throw new Exception("Invalid graph!");
            }
        }

        public List<Point> PointsForDeletedConnection(Connection firstConnection, Connection secondConnection, Point toDelete)
        {
            return firstConnection.From.Id == toDelete.Id ? new List<Point>() { secondConnection.From, firstConnection.To } : new List<Point>() { firstConnection.From, secondConnection.To };
        }
    }

    public enum Action
    {
        None,
        ReplaceEdges,
        RemovePoint,
        AddPoint,
    }
}

